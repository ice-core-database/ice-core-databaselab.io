---
layout: page
title: Analysis Visualization
---

Once we have connected to the database and pulled the raw data that we want to analyze, we can then perform any data analysis without fear of overwriting or corrupting the original data source files. Any changes that we make will only be made to the local copies on our own machines. 

In order to analyze trends in ice core data, a combination of data reduction, plotting and statistics is employed. The following pages provide code for individual tasks (i.e. plotting, resampling), as well as a process for evaluation of volcanic signatures in ice cores.