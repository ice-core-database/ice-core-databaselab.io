---
layout: page
title: Analysis Visualization
subtitle: Crustal Enrichment Factor
---

In earth science, a crustal enrichment factor (EFc) is used to differentiate between natural emissions and anthropogenic pollution (or to estimate the contributions of each). The EFc is calculated by taking the ratio of the concentration of an element of interest to the concentration of Al, Fe, or Mn of a sample, normalized for the concentration of the elements in the upper crust. The statistical significance of the EFc value depends on the sample type (i.e., air, soil, snow/ice) and the area's local history (Barbante et al., 2017; Marx et al., 2016; Pacyna & Pacyna, 2001). For most samples/elements, enrichment is seen locally, although enrichment can also occur on a regional or even global scale (Pacyna & Pacyna, 2001). Generally, an EFc of less than one is considered only to have a natural signal, and an EFc of >1.2 is deemed to have a significant anthropogenic signal (Barbante et al., 2017; Marx et al., 2016).


Requirements for the R script:
```r
tidyverse
stringr
matrixStats
dplyr
ggplot2
plotly
zoo
scales
xts
lubridate
matrixStats
```


## Calculating EFc

```r

#A function to calculate enrichment factor values for db time series objects
#rewritten as a series of dplyr mutations

EnrichmentFactor <- function(db, element, elementReference)
{
# These just have the published crustal values from two different sources
  RGppt <- read.csv('Data/CrustalStds/RudnickGao_ppt.csv')
  Wppt <- read.csv('Data/CrustalStds/Wedopohl_ppt.csv')
  
  db <- db %>% 
    CrustalEnrichmentFactor(element, elementReference, RGppt, Wppt) %>%
    VolcanicEnrichmentFactor(element, elmentReference) %>%
    AntropogenicSources(element)
  
  return(db)
}


#To Determine the Pb Contributions from Natural Sources
CrustalEnrichmentFactor <- function(db, elementX, elementXreference, RGppt, Wppt){
  #Calculate Enrichment Factor using Sr, Fe, La, Ce, Cs
  #Example: EFsr = (PRCol[Sr88]) * (Wppt[Pb]/Wppt[Sr])
  
  WpptReference <- Wppt[[elementXreference]]
  result <- db %>%
    mutate(
      #Calculate Enrichment Factor using Sr
      !! (paste(elementX,"EF.sr", sep = ".")) := (Sr88.LR.ppt) * (WpptReference/Wppt$Sr),
      #Calculate Enrichment Factor using Fe
      !! (paste(elementX,"EF.fe", sep = ".")) := (Fe56.MR.ppt) * (WpptReference/Wppt$Fe),
      #Calculate Enrichment Factor using La
      !! (paste(elementX,"EF.la", sep = ".")) := (La139.LR.ppt) * (WpptReference/Wppt$La),
      #Calculate Enrichment Factor using Ce
      !! (paste(elementX,"EF.ce", sep = ".")) := (Ce140.LR.ppt) * (WpptReference/Wppt$Ce),
      #Calculate Enrichment Factor using Cs
      !! (paste(elementX,"EF.cs", sep = ".")) := (Cs133.LR.ppt) * (WpptReference/Wppt$Cs),
    ) %>%
    #Calculate Enrichment Factor Means (Median and Arithametic Mean)
    MeanCrustalEnrichmentFactor(elementX)

  return(result);
}

VolcanicEnrichmentFactor <- function(db, elementX, elementXreference){
  #Calculate Enrichment Factor using S
  #The volcanic Pb fraction was determined using [Pb/S] volc = 7.5E -5 based on previous published values
  
  #Except for very large explosive eruptions such as Katmai in 1912 and Kuwae in 1453, where we use a [Pb/S] volc =3.5E -4 
  #based on an average value for explosive arc-type eruptions
  
  EFs <- vector()
  for(i in 1:nrow(db)){
    if((db$Time.Avg[i] > 1907 && db$Time.Avg[i] < 1917) || (db$Time.Avg[i] > 1448 && db$Time.Avg[i] < 1458)){
      EFs[i] <- (db$S32.MR.ppt[i]) * (0.00035)
    } else {
      EFs[i] <- (db$S32.MR.ppt[i]) * (0.000075)
    }
    #print(head(EFs))
  }
  db[[paste(elementX,"EF.s", sep = ".")]] <- EFs
  return(db)
}

MeanCrustalEnrichmentFactor <- function(db, elementX){
  average <- db %>% 
    select(contains("EF")) %>%
    select(!ends_with("EF.s")) %>%
    select(starts_with(elementX)) %>%
    rowMeans(na.rm = TRUE)
  
  median <- db %>% 
    select(contains("EF")) %>%
    select(!ends_with("EF.s")) %>%
    select(starts_with(elementX)) %>%
    as.matrix() %>%
    rowMedians(na.rm = TRUE)
  
  db[[(paste(elementX, "CrustalEF.avg", sep = "."))]] <- average
  db[[(paste(elementX, "CrustalEF.med", sep = "."))]] <- median
  
  return(db)
}

AntropogenicSources <- function(db, elementX){
  result <- db %>%
    mutate(
      # Total Natural Avg = CrustalEF Avg + Volcanic EF
      !! (paste(elementX,"TotalNautral.Avg", sep = ".")) := 
        (!! rlang::sym(paste(elementX, "CrustalEF.avg", sep = "."))) + (!! rlang::sym(paste(elementX, "EF.s", sep = "."))),
      # Excess Avg = Total - Total Nautral Avg
      !! (paste(elementX,"Excess.Avg", sep = ".")) := 
        (!! rlang::sym(elementX)) - ((!! rlang::sym(paste(elementX, "CrustalEF.avg", sep = "."))) + (!! rlang::sym(paste(elementX, "EF.s", sep = ".")))),
      # Total Natural Med = CrustalEF Avg + Volcanic Med
      !! (paste(elementX,"TotalNautral.Med", sep = ".")) := 
        (!! rlang::sym(paste(elementX, "CrustalEF.med", sep = "."))) + (!! rlang::sym(paste(elementX, "EF.s", sep = "."))),
      # Excess Med = Total - Total Nautral Med
      !! (paste(elementX,"Excess.Med", sep = ".")) := 
        (!! rlang::sym(elementX)) - ((!! rlang::sym(paste(elementX, "CrustalEF.med", sep = "."))) + (!! rlang::sym(paste(elementX, "EF.s", sep = "."))))
    )
  return(result)
}

```
## References
Barbante, C., Spolaor, A., Cairns, W. R., & Boutron, C. (2017). Man’s footprint on the Arctic environment as revealed by analysis of ice and snow. Earth-Science Reviews, 168, 218–231. https://doi.org/10.1016/j.earscirev.2017.02.010

Marx, S. K., Rashid, S., & Stromsoe, N. (2016). Global-scale patterns in anthropogenic Pb contamination reconstructed from natural archives. Environmental Pollution, 213, 283–298. https://doi.org/10.1016/j.envpol.2016.02.006

Pacyna, J. M., & Pacyna, E. G. (2001). An assessment of global and regional emissions of trace metals to the atmosphere from anthropogenic sources worldwide. Environmental Reviews, 9(4), 269–298. https://doi.org/10.2307/envirevi.9.4.269
