---
layout: page
title: Analysis Visualization
subtitle: Resample
---

Resampling is an important tool used by researchers to pull trends out of data by reducing the visual noise or variation. Often, ice core data is resampled to 1, 3, 5, and 10 years.

Requirements for the R script:
```r
tidyverse
stringr
matrixStats
dplyr
ggplot2
plotly
zoo
scales
xts
lubridate
```
The script begins after connection to the database has been terminated and the dataframe to be plotted is called "db".

## Resample Code
```r
#format as a xts
db <- db %>% 
  drop_na(c("Time.Avg", "Pb208.LR.ppt", "Cs133.LR.ppt", "La139.LR.ppt", "Ce140.LR.ppt", "Fe56.MR.ppt", "S32.MR.ppt", "Sr88.LR.ppt")) %>%
  select(c("Time.Avg", "Pb208.LR.ppt", "Cs133.LR.ppt", "La139.LR.ppt", "Ce140.LR.ppt", "Fe56.MR.ppt", "S32.MR.ppt", "Sr88.LR.ppt")) %>%
  #filter(Time.Avg > 0) %>%
  mutate(
    Time.Avg.POSIX = as.POSIXct(date_decimal(Time.Avg))
  )

summary(db)

#resample the data
xts_data <- as.xts(db, order.by=db$Time.Avg.POSIX) %>%
  to.period(period='years',
            k=3, #set k to be the number of years that you want to resample the data to
            indexAt ="endof",
            OHLC = FALSE)

#save dataset for future use in R
df_data <- fortify(xts_data, reclass = TRUE)
### PROBLEM EVERYTHING IS CHARACTERS!!!

### cheat to make everything numeric
#df_data_numeric <- df_data %>%
#  mutate(Pb208.LR.ppt.num = as.numeric(Pb208.LR.ppt))

sapply(c(2:9), function(n) { df_data[,n] <<- as.numeric(df_data[,n]) }, simplify=TRUE );

saveRDS(df_data, file = 'Data/db_3yr_resample.Rds')

summary(df_data)

```
