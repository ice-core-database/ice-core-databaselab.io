---
layout: page
title: What is the Denali Ice Core Project?
---
The Denali Ice Core Project is a multi-decade National Science Foundatation funded project working with ice cores from Begguya (Mt. Hunter) in Alaska. Work on the project has included PIs, graduate students, and undergraduates from the University of Maine, Dartmouth College, Colby College, University of New Hampshire, Colorado State Univerisity, Washington State University, Paul Scherrer Institute, and University of Bern.

## The Ice Cores

![A piece of the Denali ice core, being prepared for analysis](/assets/img/about/CoreSample.jpg)
*A piece of the Denali ice core, being prepared for analysis. Photo Credit: [Erin Towns](https://erintowns.me/)*

Three field seasons have been conducted at the site (62.95 °N, 151.09 °W, 3900 m asl) to collect ice cores -- 2013, 2019, and 2022. In 2013, four shallow cores were drilled to test the drilling equipment and drilling conditions (DEN-13C-1; DEN-13C-2; DEN-13C-3; DEN-13C-4). Two twin cores were drilled to bedrock (DEN-13A; DEN-13B), 211 m and 210 m respectively. Current work dates the oldest portions of these ice cores as the beginning of the Holocene (~12,000 years ago). In 2019, a 51 m core (DEN-19A) was drilled to extend the record. In 2022, two 21 m cores were drilled to sample for potential Covid-19 pandemic impacts. DEN-22A was transported frozen from the field, while DEN-22B was melted in-situ for water isotopes.

## Current Works in Progress
Due to the large scope of the Denali Ice Core Project, I am working with the collaborating institutions and researchers to build a database for the project. Check out the Denali Ice Core Database Project section for more information! Additionally, I have written parsing scripts to quickly read data from a wide variety of analytical instruments, transform them into a uniform structure, and upload them to the database. These files are freely open source and availible in the Database section via Gitlab.

For my PhD, I am currently examining the history of volcanic and anthropogenic aerosol impacts in the Arctic using Alaskan ice cores. This research is a part of a collaborative effort across New England (University of Maine, Dartmouth, Colby, and University of New Hampshire) to understand the impacts of natural and anthropogenic climate change in the North Pacific throughout the Holocene. I am working on five ice cores drilled at the Begguya site (62.95 °N, 151.09 °W, 3900 m asl) -- a test core from 2013 (DEN-13C-3), two in 2013 (DEN-13A and DEN-13B) to bedrock (211 m and 210 m), and two shallow cores (DEN-19A and DEN-22A). From these cores, I am collecting a new Pb concentration and isotope record spanning from 340 to 2022 CE. This record will allow us to trace changes in pollution sources through time as they arrive to the North Pacific, effectively tracing the changing human impact on the environment in this location. Check out the [Denali-Pb section](https://icecores.dev/src/doc/Denali-Pb/) for more details on this work!

Additionally, I am working to locate well known eruptions within the ice core in order to verify the constructed age-depth scale. This work has been completed using ice normally discarded due to contamination from drilling. The successful identification of tephra glass in the investigated layers illustrates the promise of this new method in ice-limited glaciochemistry projects.

## Why Build a Database?
Since we have been working on this project for many years and have ~20-30 currently active researchers working with the data, it is important everyone has open access to the data. Additionally, as continued refinements have been made to the depth-age scale, some older data files have become obsolete. Using a centralized database ensures that the data used by each researcher is accurate and up-to-date.

## New to Ice Cores?
Here is a quick crash course on ice core science:
 - [From drilling to data: Retrieval, transportation, analysis, and long-term storage of ice-core samples](https://doi.org/10.22498/pages.30.2.98)
 - [Putting the time in time machine: Methods to date ice cores](https://doi.org/10.22498/pages.30.2.100)
 - [Our frozen past: Ice-core insights into Earth's climate history](https://doi.org/10.22498/pages.30.2.102)
 - [Ice-core records of atmospheric composition and chemistry](https://doi.org/10.22498/pages.30.2.104)
 - [Fire trapped in ice: An introduction to biomass burning records from high-alpine and polar ice cores](https://doi.org/10.22498/pages.30.2.106)
 - [Ice-core records of human impacts on the environment](https://doi.org/10.22498/pages.30.2.108)
 - [The living record: Considerations for future biological studies of ice cores](https://doi.org/10.22498/pages.30.2.110)
 - [Firn: Applications for the interpretation of ice-core records and estimation of ice-sheet mass balance](https://doi.org/10.22498/pages.30.2.112)
 - [What can deep ice, water, sediments, and bedrock at the ice–bed interface tell us?](https://doi.org/10.22498/pages.30.2.114)
 - [Ice-core constraints on past sea-level change](https://doi.org/10.22498/pages.30.2.116)


## Terminology
**Core**: A cylinder of ice drilled from ice sheet or glacier. They are typically either 2" or 3" in diameter. Many different types of drills exist depending on the type of environment you are drilling in and how long of a core you are attempting to retrieve. [Find out more on ice core drilling here.](https://icecores.org/about-ice-cores)

**Tube (or tube meter)**: In order to extract the ice core intact and transport it safely to the lab, it must be cut to manageable lengths. Researchers aim to cut the core into roughly meter long sections, called tubes or tube meters. 

**Sample**: A sample is a small piece of the ice core that has been sent off for analysis on an analytical instrument. It may be frozen, melted water, or filtered particles. The sampling resolution will determine how many samples are run and how well you capture trends. Because time is more compressed with depth (due to pressure), maintaining a constant sampling resolution will lead to a coarser age resolution in the past and a finer resolution closer to the present.

**Certified Reference Material**: Reference materials are materials with known values which can be used as a control or standards to check the quality of products, to validate analytical measurement methods, and for the calibration of instruments.

**Depth-age scale**: Ice cores are time capsules, with compressed snow layers slowly transforming to ice under pressure. As you go down in depth, the age of the ice increases (you go back in time). Therefore, researchers have to determine what the relationship between ice depth and time (age) is. To accomplish this, they use trace elements, such as calcium and magnesium, as well as know date markers (i.e. atomic bomb tests, volcanic eruptions) to layer count the ice core.

## Acknowledgements and Funding Sources

We thank Denali National Park, Polar Field Services and Talkeetna Air Taxi for providing air support and field assistance, Mike Waszkiewicz for ice core drilling, and Brad Markle, Dave Silverstone, Tim Godaire and Elizabeth Burakowski for field assistance. We would like to particularly thank the more than 25 students who have worked on the Denali Ice Core Project for their support in the field and the lab, and to the Ice Drilling Program and Ice Core Facility for their support in retrieval and storage of the Denali Ice Cores. The work presented here was funded by the US National Science Foundation (AGS-1806422; AGS-2002483; OPP-2002470), the University of Maine Graduate Student Government, and two Graduate Maine Space Grant Consortium Fellowships

We acknowledge that Alaska Native peoples have lived on and used the land for thousands of years. Begguya lies at the intersection of the traditional lands of the Ahtna, Dena’ina, Koyukon, Upper Kuskokwim, and Tanana peoples. The Denali Ice Cores were recovered near the summit of Begguya in the Alaska Range, on this ancestral and unceded territory.

Much of the work on the Denali Ice Cores was completed at the University of Maine. The University of Maine recognizes that it is located on Marsh Island in the homeland of the Penobscot (Panawahpskek) Nation, where issues of water and territorial rights, and encroachment upon sacred sites, are ongoing. Penobscot homeland is connected to the other Wabanaki Tribal Nations — the Passamaquoddy, Maliseet, and Mi’kmaq — through kinship, alliances and diplomacy. The university also recognizes that the Penobscot Nation and the other Wabanaki Tribal Nations are distinct, sovereign, legal and political entities with their own powers of self-governance and self-determination.
