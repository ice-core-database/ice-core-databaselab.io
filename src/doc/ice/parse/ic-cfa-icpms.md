---
layout: page
title: Parsing
subtitle: Ion Chromatography, CFA and ICP-MS
---

**[(Source code hosted here)](https://gitlab.com/ice-core-database/parse-ic-cfa-icpms)**

## Purposes
This script was written for a unique set of data for the Denali Ice Core project that happened to have data from inductively coupled plasma mass spectrometry (ICP-MS), ion chromotography (IC), and continuous-flow analysis (CFA) together in a single `.csv`. The R scripts work to succently upload the data directly to a database such as PostgreSQL or Maria. 

## Functionality

The codes are written to be compatible with files produced from:

 - Thermo Scientific Element 2 high-resolution sector field ICP-MS
 - Thermo Scientific Element XR high-resolution sector field ICP-MS
 - Dionex ICS5000 capillary ion chromatograph
 - Koltz Abakus laser particle detector
 - Amber Science conductivity

The code is easily edittable to be adapted to other machine/software outputs.

### Future Work:
 - Need to write individual parse scripts that can handle each instrument analysis type individually

## Usage

This code allows IC, CFA, and ICPMS data saved as `.csv` format to be uploaded to a database with few lines of code.


## Dependencies

### R Libraries
 - tidyverse
 - DBI
 - naniar
 - progress
 - dotenv
 - purrr


## Support
If you experience issues with the code, support can be sought by emailing hanna.brooks@maine.edu.

## Authors and acknowledgment
Written by Hanna L Brooks and Camden G Bock. Last update: 2022.

## License
Code is licensed with a MIT License. See license section for more information.