---
layout: page
title: Parsing
subtitle: LA-ICP-MS with Background Removal
---

**[(Source code hosted here)](https://gitlab.com/ice-core-database/parse-laicpms)**

These python and R codes will perform preliminary data reduction for cryo-LA-ICP-MS data (i.e. background removal and signal trimming) and upload the data directly to a database such as PostgreSQL or Maria.


## Purposes
Cryo-laser ablation inductively coupled plasma mass spectrometry (cryo-LA-ICP-MS) is a novel technique which allows researchers to gather trace element data with virtually no destruction to the frozen ice core sample. While up to a meter of ice can be successfully loaded into the sample holder at a time, only 3 cm can be run at a time. In order to process the data and compare it with data from other methods, background must be removed, the signal trimmed, and scan lines assigned ice depths. These scripts work to speed up and simplify this data reduction process, eliminating human error. At the end, data is uploaded to a database such as PostgreSQL or Maria, where it can be collated with all the other data collected on the ice core.


## Functionality

The code is written to be compatible with files produced from a Thermo Scientific Element 2 ICP-MS, coupled to a New Wave UP-213 nm laser with a cryocell. Specifically, this code has been written with the UMaine W. M. Keck Laser facility in mind. The code is easily edittable to be adapted to other LA-ICP-MS machine/software outputs.


## Usage

This code requires `.csv` files that include metadata for each batch of scan lines. 



### Setting up an Environment

#### Pip

```bash
pip install -r requirements.txt
```

#### Conda

For a new environment

```bash
conda env update -n my_sem_edax_env --file ENV.yaml
```

In an existing environment (e.g., setup with PyCharm)

```bash
conda env update --file environment.yml
```

### Updating Dependencies

To publish updated environment configurations, make a conda environment YML file and a pip requirements file.

```bash
conda env export --no-builds > environment.yml

pipreqs --mode compat --use-local --force . > requirements.txt
```

## Dependencies
### Python Version

Python 3.11

### Python Libraries
 - numpy>=1.23.4
 - pandas>=1.5.1
 - psycopg2~=2.9.3
 - SQLAlchemy~=1.4.39
 - tqdm~=4.64.1
 - python-dotenv>=0.20.0
 - matplotlib>=3.6.2

### R Libraries
 - tidyverse
 - DBI
 - naniar
 - progress
 - dotenv
 - stringr
 - reshape2
 - ggplot2
 - RPostgres
 - zoo
 - cowplot
 - plotly
 - stringr


## Support
If you experience issues with the code, support can be sought by emailing hanna.brooks@maine.edu.

## Authors and acknowledgment
Written by Hanna L Brooks and Camden G Bock. Last update: 2023.

## License
Code is licensed with a MIT License. See license section for more information.