---
layout: page
title: Parsing
subtitle: Parse Git Repositories
---

In this section, there are a number of different Git repositories for uploading data from various instruments directly to a database. In each sub-section, you will find a link to the repository, as well as the readme for reference.

These scripts could be modified or adjusted to map:
 - Data from the same instruments to databases for other projects
 - Data from similiar instruments (i.e. different manufacturers with different outputs) to databases
 - Data from entirely new instruments to databases

## Coding Languages 
The scripts are a mixture of R and Python. 