---
layout: page
title: Parsing
subtitle: Normalized Microprobe
---

**[(Source code hosted here)](https://gitlab.com/ice-core-database/parse-electron-microprobe)**

These R codes allow EPMA data upload in `.csv` format directly to a database such as PostgreSQL or Maria.


## Purposes
Struggling to work with electron microprobe (EMP), electron probe microanalyzer (EPMA) or electron micro probe analyzer (EMPA) results exported as `.csv` files? These helpful scripts import all your data directly to a database such as PostgreSQL or Maria, where it can be collated with all the other data collected on the sample.

## Functionality
The code is written to be compatible with files produced from a Cameca SX-100. Specifically, this code has been written with the [(UMaine Electron Microprobe Laboratory)](https://umaine.edu/earthclimate/facilities/electron-microprobe-laboratory/) in mind. The code is easily edittable to be adapted to other EPMA machine/software outputs.

## Usage
This code requires `.csv` input of probe data.

## Dependencies

### R Libraries
 - tidyverse
 - openxlsx
 - DBI
 - dplyr
 - stringr
 - dotenv

## Support
If you experience issues with the code, support can be sought by emailing hanna.brooks@maine.edu.

## Authors and acknowledgment
Written by Hanna L Brooks and Camden G Bock. Last update: 2023.

## License
Code is licensed with a MIT License. See license section for more information.