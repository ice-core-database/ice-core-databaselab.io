---
layout: page
title: Mentorship and Outreach
---
![lab coat](/assets/img/about/LabCoat.jpg) 
*Hanna Brooks wearing a white lab coat on the campus of the University of Maine*

# Community Outreach

I am always excited by the opprotunity to talk about my research and climate science as a whole with the community! As a female scientist who identifies as disabled, I am really passionate about minority advocacy in STEM fields. I am always happy to answer questions about my research, climate science, geology, and how my disability and gender have (or have not) shaped the kind of scientist I have become. 

Over the years, I have:
- Led museum tours at Virginia Tech and the University of Alberta, showing visitors rock samples, explaining local geology, and describing the tools we use to learn about Earth's past. 
- Led lab tours at the University of Maine and Amherst College, showing visitors the analytical equipment we use to examine the chemistry of rocks, ice, and animal hard parts.
- Chatted with high schoolers about the impact of climate change on Maine.
- Worked as a volunteer scientist for [Skype-a-Scientist](https://www.skypeascientist.com/).
- Filmed episode 78 of the YouTube series ["Every Rock Has A Story"](https://www.youtube.com/watch?v=hdgLXhILAeI), talking about the Denali Ice Cores.

Please reach out if you are interested in having me talk to your community organization or classroom!

# Mentorship
Throughout grade school, I always defined myself as a “nerd”, highly focused on school. I was good at science, and often fascinated by the natural world, but also interested in history and literature. While I participated in science themed activities, such as yearly science fair projects and an internship with NASA Langley, I never thought of myself as a “scientist”. Still wondering how I fit in the world of science, I began my undergraduate at Virginia Tech studying geology and working on a metamorphic petrology research project with Dr. Mark Caddick. The transition to university was a struggle, as I was juggling a new rigor of coursework with learning to cope with a newly diagnosed brain disease with chronic neurological symptoms. Dr. Caddick saw past the uncertainty of my disease, taking me to Syros, Greece for a week of fieldwork. Returning to the lab, I examined the rock samples under the microscope to determine their composition. As I watched Dr. Caddick light up with excitement when I found unusual and unexpected minerals within the samples, I first owned the label of “scientist” for myself. This experience revolutionized my understanding of self and, ultimately, career path.

With each post-secondary research experience, I have shifted my scientific focus, from metamorphic petrology (B.S. at Virginia Tech) to analytical modeling (M.Sc. at University of Alberta) to the climate record trapped in ice cores (Ph.D. at University of Maine). With each shift, I find myself floating between communities, bridging colleagues’ topical investigations with my own interest in methodology. This is compounded with my status as a woman with a disability in the field of geosciences. My journey has been riddled with strong feelings of discomfort, inadequacy, and imposter syndrome as I search, and often fail, to find others who align with my identities and research interests, never quite meshing with a community.

I have fought against the feeling of not belonging by seeking allies in the few women in my classes and mentorship from faculty and graduate students. Straddling many community groups within the geosciences, I have observed how openness and diversity supports research efforts far beyond the individual level and how quickly poor communication and closed natures cause lasting negative effects. To overcome this, I began working to create environments where students of all abilities are encouraged, included, and emboldened. I am excited to use my position as a lab technician to guide others, as they gain ownership of the “scientist” label.

I have had the amazing privledge of mentoring several undergraduate through thesis research and independant research projects, including:
- Joshua Stone (University of Maine): U-Pb dating of accessory minerals by LA-ICP-MS/MS
- Madeline Gavin and Carina Keirstead (University of Maine): Investigation of Ancient Basal Ice from Begguya (Mt. Hunter)
- Kateu Wleh (Amherst College): Metamorphism of Kyanite Schists in the Northern Gravelly Range of SW Montana During the Paleoproterozoic Big Sky Orogeny
- Francisco Reyes (Amherst College): Reconstructing Thermobarometric Gradients in the Northwestern Tethyan Himalaya: Testing the Lithostatic Pressure Paradigm

![Josh Stone in the lab operating the LA-ICP-MS/MS at the University of Maine](/assets/img/about/IMG_20190510_160139.jpg)
*Josh Stone operating the LA-ICP-MS/MS at the University of Maine* 

![Josh Stone in the lab operating the LA-ICP-MS/MS at the University of Maine](/assets/img/about/IMG_20230414_102443135_BURST000_COVER.jpg) 
*Madeline Gavin and Carina Keirstead presenting their poster at the Student Symposium at the University of Maine*