---
layout: page
title: Hi, I'm Hanna
subtitle: Scanning Electron Microscope Technician & PhD Candidate
---

![lab coat](/assets/img/about/LabCoat.jpg) 
*Hanna Brooks wearing a white lab coat on the campus of the University of Maine*

I am interested in decoding the Earth's past by examining records left in rocks, ice, and the hard parts of animals. My current PhD research at the [University of Maine](https://climatechange.umaine.edu/people/hanna-brooks/) focuses on using glacier ice from Alaska to learn about the natural and human-caused pollution deposited through time. 

I also serve as the scanning electron microscope (SEM) technician at the [Department of Geology at Amherst College](https://www.amherst.edu/academiclife/departments/geology). In this role, I work with student and faculty researchers to unlock the chemical signatures and surface morphology recorded in their samples.

When I am not studying geochemistry, I like to crochet, go on bike rides, hang out with my dog, and [work on railroad preservation](https://www.nextgenrr.org/).

# Research Areas
As a small child growing up surrounded by the Appalachian Mountains, I quickly developed a curiosity for how the planet works. This evolved with age into a passion for geology and an undergraduate degree in geosciences from Virginia Tech. During my master’s degree at the University of Alberta, I created a model for the solubility of minerals in saline aqueous fluids in the crust and upper mantle, under the advisement of Matthew Steele-MacInnis. With goals of being a lab manager, I began pursuing a PhD at the University of Maine in 2019, advised by Karl Kreutz. I am currently finishing my PhD while working as a Scanning Electron Microscope (SEM) Technician in the [Department of Geology at Amherst College](https://www.amherst.edu/academiclife/departments/geology).

Over the years, I have amassed a fairly diverse geoscience-based scientific background, with past research projects 
spanning the fluids of subduction zones, the solubility of minerals at high pressures and temperatures, in-situ Rb-Sr and U-Pb geochronology, and the changes in chemistry of mollusc shells fish otoliths, forams, and ice cores through time.

You can browse my publications on my [ResearchGate](https://www.researchgate.net/profile/Hanna-Brooks) or 
[Google Scholar](https://scholar.google.com/citations?user=9Zh900cAAAAJ&hl=en) profiles.

## Running Scanning Electron Microscope Lab
Every day, I have the pleasure of working with undergraduate, graduate, and faculty researchers to help them capture the 
chemistry and surface features of their samples. The SEM at Amherst College is equipped with many detectors - SE, BSE, EDS,
WDS, CL, and EBSD - so users can fully quantify fine-grained details in their samples. I love that I get to work in
a wide variety of science specialities and work at the cutting edge of the STEM fields. Sample projects I have worked on
include: crystal orientations of 3D printed metals, imaging fossils, examination of lab grown crystals, detection of
contaminants on carbon mesh, and determining the mineralogy of rocks. 

## Glaciochemistry

In my PhD research, I am currently examining the history of volcanic and anthropogenic aerosol impacts in the Arctic using Alaskan ice cores. This research is a part of a collaborative effort across New England (University of Maine, Dartmouth, Colby, and University of New Hampshire) to understand the impacts of natural and anthropogenic climate change in the North Pacific throughout the Holocene. I am working on five ice cores drilled at the Begguya site (62.95 °N, 151.09 °W, 3900 m asl) -- a test core from 2013 (DEN-13C-3), two in 2013 (DEN-13A and DEN-13B) to bedrock (211 m and 210 m), and two shallow cores (DEN-19A and DEN-22A), which extend the record to 2022. For more detail, see [Denali Ice Core Project](https://icecores.dev/src/doc/ice/overview/).

![UMaine graduate students Lela Gadrani, Hanna Brooks, and Meredith Helmick work on ice cores in the freezer](/assets/img/about/Freezer.jpg)
*UMaine graduate students Lizi Gadrani, Hanna Brooks, and Meredith Helmick work on ice cores in the freezer. Photo Credit: Lizi (Lela) Gadrani*

From these cores, I am collecting a new Pb concentration and isotope record using an inductively coupled plasma mass spectrometer (ICP-MS). This record will allow us to trace changes in pollution sources through time as they arrive to the North Pacific, effectively tracing the changing human impact on the environment in this location. 

Additionally, I am working to locate well known eruptions within the ice core in order to verify the constructed age-depth scale. This work has been completed using ice normally discarded due to contamination from drilling. The successful identification of tephra glass in the investigated layers illustrates the promise of this new method in ice-limited glaciochemistry projects. 

## LA-ICP-MS on the Solid Earth
My early PhD work was focused on inductively coupled mass spectrometry (ICP-MS) and the potential this method has assist in answering many outstanding geologic questions.  My work was focused on the precise identification, dating, and characterization of past geologic and anthropogenic events in crustal rocks and ice cores. In crustal rocks, I worked on developing and improving geologic based methods applied to radiogenic dating series. Advancements in the last few years now allow for in-line chemistry to occur within a reaction cell, permitting the elimination of isotopic interferences in-situ and precise dating of beta decay chain methods (i.e. Rb-Sr) in environments previously impossible without difficult wet chemistry.