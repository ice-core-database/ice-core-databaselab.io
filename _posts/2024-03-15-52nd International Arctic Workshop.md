---
layout: post
title: 52nd International Arctic Workshop
subtitle: UMass Amherst 2024
comments: true
---

<iframe src="https://slides.com/hannabrooks/geolunch-2024/embed" width="800%" height="80%" title="52nd International Arctic Workshop" scrolling="no" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>